#ifndef _ASMROUTINE_
#define _ASMROUTINE_
//Общие определения
#ifdef __ASSEMBLER__
//Определения только для ассемблера
#endif
#ifndef __ASSEMBLER__
//Определения только для С
extern "C" {
  char delay_asm_low();
  char delay_asm_medium();
  char delay_asm_high();
}
#endif
#endif