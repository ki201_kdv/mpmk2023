//Лістинг 6.1 – Головний файл
#include <avr/io.h>
#include "asmroutine.h"

int val = 0;
int pwm = 0;
void setup() {
  Serial.begin(9600); 
  Serial.print("PWM-Asembler:\n");
  Serial.print("  LOW level is 1;\n");
  Serial.print("  Medium level is 2;\n");
  Serial.print("  HIGH level is 3;\n");
  Serial.print("\n\n");
}

void loop() {
  val = Serial.parseInt();
  if(val == 1 || pwm == 1){
    Serial.print("level: low\n");
    pwm = 1;
    delay_asm_low();
  }
  if(val == 2 || pwm == 2){
    Serial.print("level: medium\n");
    pwm = 2;
    delay_asm_medium();
  }
  if(val == 3 || pwm == 3){
    Serial.print("level: high\n");
    pwm = 3;
    delay_asm_high();
  }
  if(val > 3){
    Serial.print("Error input value. Try again.");
  }
}
