int input = 7; //Запис в регістр
int output = 4; //Вивід з регістру
int level = 8; //Рівень запису сигналу
int val = 0; //Користувацьке значення (зчитується монітором серійного порта)
int ii = 0;

//Цифри
int minus[8] = {1,1,0,0,0,0,0,0};
int one[8] = {0,0,0,0,0,1,1,0};
int two[8] = {0,1,0,1,1,0,1,1};
int three[8] = {0,1,0,0,1,1,1,1};
int four[8] = {0,1,1,0,0,1,1,0};
int five[8] = {0,1,1,0,1,1,0,1};
int six[8] = {0,1,1,1,1,1,0,1};
int seven[8] = {0,0,0,0,0,1,1,1};
int eight[8] = {0,1,1,1,1,1,1,1};
int nine[8] = {0,1,1,0,1,1,1,1};
int zero[8] = {0,0,1,1,1,1,1,1};

void setup()
{
  pinMode(input, OUTPUT); //Ініціалізацію інтерфейса на вивід
  pinMode(output, OUTPUT); //Ініціалізацію інтерфейса на вивід
  pinMode(level, OUTPUT); //Ініціалізацію інтерфейса на вивід
  Serial.begin(9600); //Ініціалізацію серійного порта
}

void loop(){
  val = Serial.read();
  timer(val);
  numbers(minus);
  delay(250);
}

void timer(int n){
  n = n - 48;
    for (int i = n; i >= 0; i--){
      if(i == 1){
      	numbers(one);
        delay(1000);
      }
      if(i == 2){
      	numbers(two);
        delay(1000);
      }
      if(i == 3){
      	numbers(three);
        delay(1000);
      }
      if(i == 4){
      	numbers(four);
        delay(1000);
      }
      if(i == 5){
      	numbers(five);
        delay(1000);
      }
      if(i == 6){
      	numbers(six);
        delay(1000);
      }
      if(i == 7){
      	numbers(seven);
        delay(1000);
      }
      if(i == 8){
      	numbers(eight);
        delay(1000);
      }
      if(i == 9){
      	numbers(nine);
        delay(1000);
      }
    }
}

void numbers(int num[]){ //Функція для відображення значення на семисигментному індикаторі
  for (ii = 0; ii < 8; ii++){
      if(num[ii] == 1){
        digitalWrite(level, HIGH);
        digitalWrite(input, HIGH);
        digitalWrite(input, LOW);
        digitalWrite(output, HIGH);
        digitalWrite(output, LOW);
        digitalWrite(level, LOW);
      }
      else{    
        digitalWrite(input, HIGH);
        digitalWrite(input, LOW);
        digitalWrite(output, HIGH);
        digitalWrite(output, LOW);
      }
   }
}