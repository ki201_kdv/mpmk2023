//Лістинг 6.1 – Головний файл
#include <avr/io.h>
/*Подключаем заголовочный файл, в котором объявлена 
наша функция (и, если необходимо, еще что-то)*/
#include "asmroutine.h"

volatile char d;

void setup() {
  volatile char m = 5;
  Serial.begin(9600);
  /*Вызываем ассемблерную функцию, которая вернет
  сумму своего параметра и глобальной переменной.*/
  d = asm_func(m);
}

void loop() {
  // put your main code here, to run repeatedly:
  if (d==10){
    Serial.println("Ok");
  }
}