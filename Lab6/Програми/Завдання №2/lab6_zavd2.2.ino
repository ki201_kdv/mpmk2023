//Мигание светодиодом с использованием ассемблера (для Arduino UNO)
asm volatile (
  ".equ DDRD, 0x0A ;Адрес регистра направления данных порта D \n"
  ".equ PORTD,0x0B ;Адрес регистра вывода данных порта D \n"
  ".equ PIND, 0x09 ;Адрес регистра ввода данных порта D \n"
  ".equ LED, 0b00000100 ;Бит 2 порта D ооответствует биту D2 Arduino UNO \n"
);
void setup() {
// Инициализация порта D (бит 2 порта D на выход)
  asm volatile(
    "ldi r16,LED ; сохраняем константу в регистре r16 \n"
    "out DDRD,r16 ; Отправляем значение в DDRD \n"
  );
}
void loop() {
// Включение светодиода
  asm volatile (
    "ldi r16,LED \n"
    "out PORTD,r16 \n"
  );
  delay(1000);
  // Выключение светодиода
  asm volatile (
    "ldi r16,0 \n"
    "out PORTD,r16 \n"
  );
  delay(1000);
}