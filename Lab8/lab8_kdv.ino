#include <LiquidCrystal_I2C.h>

#include <Wire.h>

#include <Servo.h>
int val_x1; //показник з фоторезистора X1
int val_x2; //показник з фоторезистора X2
int val_y1; //показник з фоторезистора Y1
int val_y2; //показник з фоторезистора Y2
int c_serv = 7; //пін для контролю живлення сервомоторів
int led_r = 4;  //пін для індикації роботи схеми(червоний)
int led_g = 5;  //пін для індикації роботи схеми(зелений)
int button = 12;  //Елемент керування схемою у вигляді кнопки
int Angle_Servo_X = 0;
int Angle_Servo_Y = 0;
Servo servo_x;
Servo servo_y;

void setup()
{
  pinMode(button, INPUT);  //Ініціювання відповідних режимі роботи потртів
  pinMode(led_r, OUTPUT);  
  pinMode(led_g, OUTPUT);
  pinMode(c_serv, OUTPUT);
  pinMode(A0, INPUT);
  pinMode(A1, INPUT);
  pinMode(A2, INPUT);
  pinMode(A3, INPUT);
  digitalWrite(led_r, HIGH);  //Початкова активація червоного світлодіоду, так як повинен виконатися процес калібрування
  servo_x.attach(10); //Визначаємо порт до якого підключено сервомотор по горизонталі
  servo_y.attach(9); //Визначаємо порт до якого підключено сервомотор по вертикалі
  Serial.begin(9600); //Ініціалізація серійного порту для моніторингу роботи схеми
  calebration(); //Виклик функції калібрування
}

void loop()
{
  if(digitalRead(button) == 1){ //Перевірка, якщо кнопка натиснута, то виконувати калібрування
  	calebration();
  }
  val_x1 = map(analogRead(A0), 0, 1023, 0, 256); //Зчитування значень з датчику X1
  val_x2 = map(analogRead(A1), 0, 1023, 0, 256); //Зчитування значень з датчику X2
  val_y1 = map(analogRead(A2), 0, 1023, 0, 256);; //Зчитування значень з датчику Y1
  val_y2 = map(analogRead(A3), 0, 1023, 0, 256); //Зчитування значень з датчику Y2
  //У циклій постійно опитуємо датчики, та корегуємо положення сервомотора на 10 градусів (значення можна відрегулювати)
  if(val_x1 > val_x2){  //Якщо значення сервомотора X1 > X2, то руїамо мотор по годинниковій стрілці на 10 градусів, до моменту поки значення не вирівняються
	Angle_Servo_X = Angle_Servo_X + 5;
    digitalWrite(c_serv, HIGH);
    servo_x.write(Angle_Servo_X);
    delay(100);
    digitalWrite(c_serv, LOW);
  };
  if(val_x2 > val_x1){ //Якщо значення сервомотора X2 > X1, то руїамо мотор проти годинниковій стрілці на 10 градусів, до моменту поки значення не вирівняються
	Angle_Servo_X = Angle_Servo_X - 5;
    digitalWrite(c_serv, HIGH);
    servo_x.write(Angle_Servo_X);
    delay(100);
    digitalWrite(c_serv, LOW);
  };
  if(val_y1 > val_y2){ //Аналогічні перевірки до значень по горизонту.
	Angle_Servo_Y = Angle_Servo_Y + 5;
    digitalWrite(c_serv, HIGH);
    servo_y.write(Angle_Servo_Y);
    delay(100);
    digitalWrite(c_serv, LOW);
  };
  if(val_y2 > val_y1){ //Аналогічні перевірки до значень по горизонту.
 	Angle_Servo_Y = Angle_Servo_Y - 5;
    digitalWrite(c_serv, HIGH);
    servo_y.write(Angle_Servo_Y);
    delay(100);
    digitalWrite(c_serv, LOW);
  };
  if(val_x1 <= 10 && val_x2 <= 10 || val_y1 <= 10 && val_y2 <= 10){
  	digitalWrite(led_g, LOW);
    digitalWrite(led_r, HIGH);
  }
  if(val_x1 > 10 && val_x2 > 10 || val_y1 > 10 && val_y2 > 10){
    digitalWrite(led_g, HIGH);
    digitalWrite(led_r, LOW);
  }
}


void calebration(){
  digitalWrite(led_g, LOW);
  digitalWrite(led_r, HIGH);
  tone(3, 200, 300);  //Керування спікером, для сигналізування режиму калібрування
  delay(500);
  tone(3, 200, 300);
  delay(500);
  tone(3, 200, 500);
  digitalWrite(c_serv, HIGH); //Увімкнення живлення сервомоторів
  int max = 0;
  int angle_x = 0;
  int angle_y = 0;
  for (int i = 0; i <= 18; i++){
  	//Калібрування по горизонту
    val_x1 = analogRead(A0); //Зчитування значень з датчику X1
    if (val_x1 >= max){
    	max = val_x1;
      	Angle_Servo_X = angle_x;
      	angle_x = angle_x + 10;
      	servo_x.write(angle_x);
    }
    else{
    	break;
    }
    delay(1000);
  }
  max = 0;
  servo_x.write(Angle_Servo_X);
  for (int i = 0; i <= 18; i++){ 
  	//Калібрування по вертикалі
    val_y1 = analogRead(A2); //Зчитування значень з датчику X1
    if (val_y1 >= max){
      max = val_y1;
      Angle_Servo_Y = angle_y;
      angle_y = angle_y + 10;
      servo_y.write(angle_y);
    }
    else{
    	break;
    }
    delay(1000);
  } 
  servo_x.write(Angle_Servo_Y);
  digitalWrite(c_serv, LOW);  //Вимкнення живлення сервомоторів
  Serial.print("Калібрування завершено.\n Оптимальні кути - \n   x - ");
  Serial.print(Angle_Servo_X);
  Serial.print(";\n   y - ");
  Serial.print(Angle_Servo_Y);
  Serial.print(";\n");
  digitalWrite(led_r, LOW);
  digitalWrite(led_g, HIGH);
}
