print("Програма реалізації логічної функції - \"F = x1!x3 + !x1x3x4 + !x2!x3!x4\":")
print("Для продовження роботи з програмою введіть значення відповідних змінних (1 або 0): ")
test = True

x1 = input("Введіть значення x1: ")
x2 = input("Введіть значення x2: ")
x3 = input("Введіть значення x3: ")
x4 = input("Введіть значення x4: ")


if int(x1) == 1:
    x1 = True
else:
    x1 = False
if int(x2) == 1:
    x2 = True
else:
    x2 = False
if int(x3) == 1:
    x3 = True
else:
    x3 = False
if int(x4) == 1:
    x4 = True
else:
    x4 = False

nx1 = (not x1)
nx2 = (not x2)
nx3 = (not x3)
nx4 = (not x4)

funk = ((x1 and nx3) or (nx1 and x3 and x4) or (nx2 and nx3 and nx4))
print("Результат роботи функції - " + str(funk))

