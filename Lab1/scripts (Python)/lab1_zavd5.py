print("Програма реалізації роботи схеми лічильника на основі графа.")
i = 0
lim = 10


for i in range (lim):
    print("Код на вході: " + "{0:05b}".format(i))
    val = str("{0:05b}".format(i))
    x1 = val[4]
    x2 = val[3]
    x3 = val[2]
    x4 = val[1]
    x5 = val[0]

    if int(x1) == 1:
        x1 = True
    else:
        x1 = False
    if int(x2) == 1:
        x2 = True
    else:
        x2 = False
    if int(x3) == 1:
        x3 = True
    else:
        x3 = False
    if int(x4) == 1:
        x4 = True
    else:
        x4 = False
    if int(x5) == 1:
        x5 = True
    else:
        x5 = False

    nx1 = (not x1)
    nx2 = (not x2)
    nx3 = (not x3)
    nx4 = (not x4)
    nx5 = (not x5)

    d1 = ((nx1 and nx2 and nx3 and nx4) or (x1 and x2 and nx3 and nx4))
    d2 = ((nx1 and nx3 and nx4) or (x4 and nx5) or (nx1 and x2 and nx4 and nx5))
    d3 = ((nx2 and nx3 and nx4) or (nx1 and x4 and nx5) or (x1 and nx2 and x3 and nx4))
    d4 = ((nx3 and nx4) or (x4 and nx5) or (nx1 and nx2 and x3 and nx4))
    d5 = (nx4 and nx5)

    if (d1 == True):
        d1 = 1
    else:
        d1 = 0

    if (d2 == True):
        d2 = 1
    else:
        d2 = 0

    if (d3 == True):
        d3 = 1
    else:
        d3 = 0

    if (d4 == True):
        d4 = 1
    else:
        d4 = 0

    if (d5 == True):
        d5 = 1
    else:
        d5 = 0
    print("Вихідний результат(F1,F2,F3,F4,F5): " + str(d5) + str(d4) + str(d3) + str(d2) + str(d1))
    print("-----------------------------------")
    i = i + 1




