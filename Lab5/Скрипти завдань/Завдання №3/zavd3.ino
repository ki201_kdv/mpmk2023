int input = 7;
int output = 4;
int level = 8;
int i = 0;
int zero[8] = {0,0,1,1,1,1,1,1}; // Dot, G, F, E, D, C, B, A
int one[8] = {0,0,0,0,0,1,1,0}; // Dot, G, F, E, D, C, B, A
int two[8] = {0,1,0,1,1,0,1,1}; // Dot, G, F, E, D, C, B, A
int three[8] = {0,1,0,0,1,1,1,1}; // Dot, G, F, E, D, C, B, A
int four[8] = {0,1,1,0,0,1,1,0}; // Dot, G, F, E, D, C, B, A
int five[8] = {0,1,1,0,1,1,0,1}; // Dot, G, F, E, D, C, B, A
int six[8] = {0,1,1,1,1,1,0,1}; // Dot, G, F, E, D, C, B, A
int seven[8] = {0,0,0,0,0,1,1,1}; // Dot, G, F, E, D, C, B, A
int eight[8] = {0,1,1,1,1,1,1,1}; // Dot, G, F, E, D, C, B, A
int nine[8] = {0,1,1,0,1,1,1,1}; // Dot, G, F, E, D, C, B, A

void setup()
{
  pinMode(input, OUTPUT);
  pinMode(output, OUTPUT);
  pinMode(level, OUTPUT);
  Serial.begin(9600);
}

void loop(){
  
  numbers(one);
  delay(1000);
  numbers(two);
  delay(1000);
  numbers(three);
  delay(1000);
  numbers(four);
  delay(1000);
  numbers(five);
  delay(1000);
  numbers(six);
  delay(1000);
  numbers(seven);
  delay(1000);
  numbers(eight);
  delay(1000);
  numbers(nine);
  delay(1000);
  numbers(zero);
  delay(1000);
}

void numbers(int num[]){
  for (i = 0; i < 8; i++)
    {
      if(num[i] == 1){
        digitalWrite(level, HIGH);
        digitalWrite(input, HIGH);
        digitalWrite(input, LOW);
        digitalWrite(output, HIGH);
        digitalWrite(output, LOW);
        digitalWrite(level, LOW);
      }
      else{    
        digitalWrite(input, HIGH);
        digitalWrite(input, LOW);
        digitalWrite(output, HIGH);
        digitalWrite(output, LOW);
      }
    };
}
