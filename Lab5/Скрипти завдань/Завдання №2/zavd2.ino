int input = 7;
int output = 4;
int level = 8;
int i = 0;
int point1[8] = {1,0,0,1,1,0,0,1};
int point2[8] = {1,0,1,1,1,0,1,1};
int point3[8] = {1,1,0,1,1,1,0,1};
int point4[8] = {0,1,1,0,0,1,1,0};

void setup()
{
  pinMode(input, OUTPUT);
  pinMode(output, OUTPUT);
  pinMode(level, OUTPUT);
  Serial.begin(9600);
}

void loop(){
	for (i = 0; i < 8; i++)
    {
      if(point1[i] == 1){
        digitalWrite(level, HIGH);
        digitalWrite(input, HIGH);
        delay(100);
        digitalWrite(input, LOW);
        digitalWrite(output, HIGH);
        delay(100);
        digitalWrite(output, LOW);
        digitalWrite(level, LOW);
        Serial.print("1\n");
      }
      else{
        digitalWrite(input, HIGH);
        delay(100);
        digitalWrite(input, LOW);
        digitalWrite(output, HIGH);
        delay(100);
        digitalWrite(output, LOW);
        Serial.print("0\n");
      }
    };
 Serial.print("------Point1-----\n");
 delay(250);
  
  	for (i = 0; i < 8; i++)
    {
      if(point2[i] == 1){
        digitalWrite(level, HIGH);
        digitalWrite(input, HIGH);
        delay(100);
        digitalWrite(input, LOW);
        digitalWrite(output, HIGH);
        delay(100);
        digitalWrite(output, LOW);
        digitalWrite(level, LOW);
        Serial.print("1\n");
      }
      else{
        digitalWrite(input, HIGH);
        delay(100);
        digitalWrite(input, LOW);
        digitalWrite(output, HIGH);
        delay(100);
        digitalWrite(output, LOW);
        Serial.print("0\n");
      }
    };
  Serial.print("------Point2-----\n");
  delay(250);
  
  	for (i = 0; i < 8; i++)
    {
      if(point3[i] == 1){
        digitalWrite(level, HIGH);
        digitalWrite(input, HIGH);
        delay(100);
        digitalWrite(input, LOW);
        digitalWrite(output, HIGH);
        delay(100);
        digitalWrite(output, LOW);
        digitalWrite(level, LOW);
        Serial.print("1\n");
      }
      else{
        digitalWrite(input, HIGH);
        delay(100);
        digitalWrite(input, LOW);
        digitalWrite(output, HIGH);
        delay(100);
        digitalWrite(output, LOW);
        Serial.print("0\n");
      }
    };
  Serial.print("------Point3-----\n");
  delay(250);
  
  	for (i = 0; i < 8; i++)
    {
      if(point4[i] == 1){
        digitalWrite(level, HIGH);
        digitalWrite(input, HIGH);
        delay(100);
        digitalWrite(input, LOW);
        digitalWrite(output, HIGH);
        delay(100);
        digitalWrite(output, LOW);
        digitalWrite(level, LOW);
        Serial.print("1\n");
      }
      else{
        digitalWrite(input, HIGH);
        delay(100);
        digitalWrite(input, LOW);
        digitalWrite(output, HIGH);
        delay(100);
        digitalWrite(output, LOW);
        Serial.print("0\n");
      }
    };
  Serial.print("------Point4-----\n");
  delay(25000);
}


