int input = 7; //Запис в регістр
int output = 4; //Вивід з регістру
int level = 8; //Рівень запису сигналу
int button = 12; //Змінна кнопки
int jj = 1; // Змінна для визначення порядкового номеру символа
int ii = 0; //Ідентифікатор часу натисання на кнопку
//Службові символи
int off[8] = {0,0,0,0,0,0,0,0};
int aa[8] = {1,0,0,0,0,0,0,0};
int ab[8] = {0,1,0,0,0,0,0,0};

//Англійський алфавіт
int a[8] = {0,1,1,1,0,1,1,1};
int b[8] = {0,1,1,1,1,1,0,0};
int c[8] = {0,0,1,1,1,0,0,1};
int d[8] = {0,1,0,1,1,1,1,0};
int e[8] = {0,1,1,1,1,0,0,1};
int f[8] = {0,1,1,1,0,0,0,1};
int g[8] = {0,1,1,0,1,1,1,1};
int h[8] = {0,1,1,1,0,1,0,0};
int i[8] = {0,0,0,0,0,1,0,0};
int j[8] = {0,0,0,1,1,1,1,0};
int k[8] = {0,1,1,1,0,1,1,0};
int l[8] = {0,0,1,1,1,0,0,0};
int m[8] = {0,0,0,1,0,1,0,1};
int n[8] = {0,1,0,1,0,1,0,0};
int o[8] = {0,1,0,1,1,1,0,0};
int p[8] = {0,1,1,1,0,0,1,1};
int q[8] = {0,1,1,0,0,1,1,1};
int r[8] = {0,1,0,1,0,0,0,0};
int s[8] = {0,1,1,0,1,1,0,1};
int t[8] = {0,1,1,1,1,0,0,0};
int u[8] = {0,0,1,1,1,1,1,0};
int v[8] = {0,0,0,1,1,1,0,0};
int w[8] = {0,0,1,0,1,0,1,0};
int x[8] = {0,1,1,1,0,1,1,0};
int y[8] = {0,1,1,0,1,1,1,0};
int z[8] = {0,1,0,1,1,0,1,1};

//Цифри
int one[8] = {0,0,0,0,0,1,1,0};
int two[8] = {0,1,0,1,1,0,1,1};
int three[8] = {0,1,0,0,1,1,1,1};
int four[8] = {0,1,1,0,0,1,1,0};
int five[8] = {0,1,1,0,1,1,0,1};
int six[8] = {0,1,1,1,1,1,0,1};
int seven[8] = {0,0,0,0,0,1,1,1};
int eight[8] = {0,1,1,1,1,1,1,1};
int nine[8] = {0,1,1,0,1,1,1,1};
int zero[8] = {0,0,1,1,1,1,1,1};


//Змінні для зберігання кодування символів абетки Морзе
char symbol1 = ' ';
char symbol2 = ' ';
char symbol3 = ' ';
char symbol4 = ' ';
char symbol5 = ' ';

void setup()
{
  pinMode(input, OUTPUT); //Ініціалізацію інтерфейса на вивід
  pinMode(output, OUTPUT); //Ініціалізацію інтерфейса на вивід
  pinMode(level, OUTPUT); //Ініціалізацію інтерфейса на вивід
  pinMode(button, INPUT); //Ініціалізацію інтерфейса на ввід
  Serial.begin(9600); //Ініціалізацію серійного порта
}

void loop(){
    Serial.print("------\n symb1:");
    Serial.print(symbol1); //вивід першого символа
    Serial.print("---\n symb2:");
    Serial.print(symbol2); //вивід другого символа
    Serial.print("---\n symb3:");
    Serial.print(symbol3); //вивід третього символа
    Serial.print("---\n symb4:");
    Serial.print(symbol4); //вивід четвертого символа
    Serial.print("---\n symb5:");
    Serial.print(symbol5); //вивід п'ятого символа
    Serial.print("---\n------\nj = ");
    Serial.print(jj); //вивід порядкового номеру символа, що вводиться
    Serial.print("\ni = ");
    Serial.print(ii); //вивід змінної часу натискання на кнопку
    Serial.print("\n");
    
    while(digitalRead(button) == 1){ //Цикл перевірки натискання на кнопку
      ii = ii + 2;
      Serial.print(ii);
      Serial.print("\n");
    }
    
    if(ii >= 90 && ii <= 110){ //Умова перевірки для визначення символу "."
      numbers(aa);
      Serial.print("  .  \n");
      write('.', jj);
      ii = 0;
    }
    
    if(ii >= 270 && ii <= 330){ //Умова перевірки для визначення символу "-"
      numbers(ab);
      Serial.print("  -  \n");
      write('-', jj);
      ii = 0;
    }
    if(ii != 0 && ii <= 89){ //Умова перевірки виклику помилки
    	error();
    }
    else{
          if(ii >= 111 && ii <= 269){ //Умова перевірки виклику помилки
            error();
       		}
          	else{
            	if(ii >= 331 && ii <= 990){ //Умова перевірки виклику помилки
                error(); 
              }
              else{
                if(ii >= 991 && ii <= 1499){ //Умова перевірки виклику функції Morze після затискання на кнопку вводу
                  Morze();
                }
                else{
                   if(ii > 1500){ //Умова перевірки виклику функції reset, для скидання параметрів
                    reset();
                   }   
               	}
              
              }
            }
         }
    if(jj >= 6){ //Умова перевірки переповнення змінної порядкового номера символу
 	    jj = 0;
      	Morze();
    }
}

void reset(){ //Функція для скидання параметрів
  Serial.print("\n\nRESET\n");
  numbers(r);
  delay(750);
  numbers(off);
  delay(100);
  symbol1 = ' ';
  symbol2 = ' ';
  symbol3 = ' ';
  symbol4 = ' ';
  symbol5 = ' ';
  jj = 1;
  ii = 0;
}

void error(){ //Функція для відображення помилковиго вводу даних
  Serial.print("\n\nERROR\n");
	numbers(e);
 	delay(750);
	numbers(off);
	delay(100);
	numbers(r);
	delay(750);
  numbers(off);
  delay(100);
 	numbers(r);
 	delay(750);
 	numbers(off);
 	delay(100);
  ii = 0;
}

void numbers(int num[]){ //Функція для відображення значення на семисигментному індикаторі
  for (ii = 0; ii < 8; ii++){
      if(num[ii] == 1){
        digitalWrite(level, HIGH);
        digitalWrite(input, HIGH);
        digitalWrite(input, LOW);
        digitalWrite(output, HIGH);
        digitalWrite(output, LOW);
        digitalWrite(level, LOW);
      }
      else{    
        digitalWrite(input, HIGH);
        digitalWrite(input, LOW);
        digitalWrite(output, HIGH);
        digitalWrite(output, LOW);
      }
   }
}

void write(char symb, int hh){ //Функція записування значення символа
  Serial.print("\n\nWRITE\n");
  delay(1000);
  Serial.print(symb);
  delay(1000);
  Serial.print(hh);
  delay(1000);
    if(hh == 1){
      symbol1 = symb;
      Serial.print("1\n");
      Serial.print(symb);
      jj++;
    }
    if(hh == 2){
      symbol2 = symb;
      Serial.print("2\n");
      Serial.print(symb);
      jj++;
    }
    if(hh == 3){
      symbol3 = symb;
      Serial.print("3\n");
      Serial.print(symb);
      jj++;
    }
    if(hh == 4){
      symbol4 = symb;
      Serial.print("4\n");
      Serial.print(symb);
      jj++;
    }
    if(hh == 5){
      symbol5 = symb;
      Serial.print("5\n");
      Serial.print(symb);
      jj++;
    }
}

void Morze(){ //Функція перевірки відповідності символа до абетки Морзе
  Serial.print("Printing...");
  if(symbol1 == '.' && symbol2 == '-' && symbol3 == ' ' && symbol4 == ' ' && symbol5 == ' '){
    numbers(a);
  }
  if(symbol1 == '-' && symbol2 == '.' && symbol3 == '.' && symbol4 == '.' && symbol5 == ' '){
    numbers(b);
  }
  if(symbol1 == '-' && symbol2 == '.' && symbol3 == '-' && symbol4 == '.' && symbol5 == ' '){
    numbers(c);
  }
  if(symbol1 == '-' && symbol2 == '.' && symbol3 == '.' && symbol4 == ' ' && symbol5 == ' '){
    numbers(d);
  }
  if(symbol1 == '.' && symbol2 == ' ' && symbol3 == ' ' && symbol4 == ' ' && symbol5 == ' '){
    numbers(e);
  }
  if(symbol1 == '.' && symbol2 == '.' && symbol3 == '-' && symbol4 == '.' && symbol5 == ' '){
    numbers(f);
  }
  if(symbol1 == '-' && symbol2 == '-' && symbol3 == '.' && symbol4 == ' ' && symbol5 == ' '){
    numbers(g);
  }
  if(symbol1 == '.' && symbol2 == '.' && symbol3 == '.' && symbol4 == '.' && symbol5 == ' '){
    numbers(h);
  }
  if(symbol1 == '.' && symbol2 == '.' && symbol3 == ' ' && symbol4 == ' ' && symbol5 == ' '){
    numbers(i);
  }
  if(symbol1 == '.' && symbol2 == '-' && symbol3 == '-' && symbol4 == '-' && symbol5 == ' '){
    numbers(j);
  }
  if(symbol1 == '-' && symbol2 == '.' && symbol3 == '-' && symbol4 == ' ' && symbol5 == ' '){
    numbers(k);
  }
  if(symbol1 == '.' && symbol2 == '-' && symbol3 == '.' && symbol4 == '.' && symbol5 == ' '){
    numbers(l);
  }
  if(symbol1 == '-' && symbol2 == '-' && symbol3 == ' ' && symbol4 == ' ' && symbol5 == ' '){
    numbers(m);
  }
  if(symbol1 == '-' && symbol2 == '.' && symbol3 == ' ' && symbol4 == ' ' && symbol5 == ' '){
    numbers(n);
  }
  if(symbol1 == '-' && symbol2 == '-' && symbol3 == '-' && symbol4 == ' ' && symbol5 == ' '){
    numbers(o);
  }
  if(symbol1 == '.' && symbol2 == '-' && symbol3 == '-' && symbol4 == '.' && symbol5 == ' '){
    numbers(p);
  }
  if(symbol1 == '-' && symbol2 == '-' && symbol3 == '.' && symbol4 == '-' && symbol5 == ' '){
    numbers(q);
  }
  if(symbol1 == '.' && symbol2 == '-' && symbol3 == '.' && symbol4 == ' ' && symbol5 == ' '){
    numbers(r);
  }
  if(symbol1 == '.' && symbol2 == '.' && symbol3 == '.' && symbol4 == ' ' && symbol5 == ' '){
    numbers(s);
  }
  if(symbol1 == '-' && symbol2 == ' ' && symbol3 == ' ' && symbol4 == ' ' && symbol5 == ' '){
    numbers(t);
  }
  if(symbol1 == '.' && symbol2 == '.' && symbol3 == '-' && symbol4 == ' ' && symbol5 == ' '){
    numbers(u);
  }
  if(symbol1 == '.' && symbol2 == '.' && symbol3 == '.' && symbol4 == '-' && symbol5 == ' '){
    numbers(v);
  }
  if(symbol1 == '.' && symbol2 == '-' && symbol3 == '-' && symbol4 == ' ' && symbol5 == ' '){
    numbers(w);
  }
  if(symbol1 == '-' && symbol2 == '.' && symbol3 == '.' && symbol4 == '-' && symbol5 == ' '){
    numbers(x);
  }
  if(symbol1 == '-' && symbol2 == '.' && symbol3 == '-' && symbol4 == '-' && symbol5 == ' '){
    numbers(y);
  }
  if(symbol1 == '-' && symbol2 == '-' && symbol3 == '.' && symbol4 == '.' && symbol5 == ' '){
    numbers(z);
  }

  //Цифри
  if(symbol1 == '.' && symbol2 == '-' && symbol3 == '-' && symbol4 == '-' && symbol5 == '-'){
    numbers(one);
  }
  if(symbol1 == '.' && symbol2 == '.' && symbol3 == '-' && symbol4 == '-' && symbol5 == '-'){
    numbers(two);
  }
  if(symbol1 == '.' && symbol2 == '.' && symbol3 == '.' && symbol4 == '-' && symbol5 == '-'){
    numbers(three);
  }
  if(symbol1 == '.' && symbol2 == '.' && symbol3 == '.' && symbol4 == '.' && symbol5 == '-'){
    numbers(four);
  }
  if(symbol1 == '.' && symbol2 == '.' && symbol3 == '.' && symbol4 == '.' && symbol5 == '.'){
    numbers(five);
  }
  if(symbol1 == '-' && symbol2 == '.' && symbol3 == '.' && symbol4 == '.' && symbol5 == '.'){
    numbers(six);
  }
  if(symbol1 == '-' && symbol2 == '-' && symbol3 == '.' && symbol4 == '.' && symbol5 == '.'){
    numbers(seven);
  }
  if(symbol1 == '-' && symbol2 == '-' && symbol3 == '-' && symbol4 == '.' && symbol5 == '.'){
    numbers(eight);
  }
  if(symbol1 == '-' && symbol2 == '-' && symbol3 == '-' && symbol4 == '-' && symbol5 == '.'){
    numbers(nine);
  }
  if(symbol1 == '-' && symbol2 == '-' && symbol3 == '-' && symbol4 == '-' && symbol5 == '-'){
    numbers(zero);
  }
  symbol1 = ' ';
  symbol2 = ' ';
  symbol3 = ' ';
  symbol4 = ' ';
  symbol5 = ' ';
  jj = 1;
  ii = 0;
  delay(2000);
}
