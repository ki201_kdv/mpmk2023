unsigned short add(unsigned char val1, unsigned char val2) {
  unsigned short res;
  asm volatile (
    "add %1, %2 \n"              // Додає значення val2 до val1
    "ldi r16, 0 \n"              // Завантажує нульове значення в регістр r16
    "adc r16, __zero_reg__ \n"   // Виконує додавання з переносом між регістрами r16 та __zero_reg__
    "mov %A0, %1 \n"             // Копіює нижній байт результуючого значення до змінної res
    "mov %B0, r16 \n"            // Копіює верхній байт результуючого значення до змінної res
    : "=&w"  (res)               // Визначення вивідного операнду (res) та типу операнду
    : "r" (val1), "r" (val2)     // Визначення вхідних операндів (val1, val2) та типів операндів
    : "r16"                      // Перелік регістрів, що використовуються в коді
   );
  return res;
}

void setup(){
  Serial.begin(9600);
}

void loop(){  //Ввод значень для додавання виконується з клавіатури через монітор серійного порту.
  Serial.print("\n--------------------");
  Serial.print("\nNumber 1: ");
  while(!Serial.available()) {}
  unsigned char val1 = Serial.parseInt();
  Serial.print(val1);
  Serial.print("\nNumber 2: ");
  while(!Serial.available()) {}
  unsigned char val2 = Serial.parseInt();
  Serial.print(val2);
  Serial.print("\nSumm is: ");
  Serial.print(add(val1, val2));
  delay(500);
}