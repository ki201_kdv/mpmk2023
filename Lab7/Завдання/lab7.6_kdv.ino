unsigned short asm_func(unsigned char numb1, unsigned char numb2){
  unsigned short result;
  asm volatile(
    "mul %1, %2\n"      // Виконуємо операцію множення чисел numb1 і numb2
    "mov r26, r0\n"     // Переміщуємо молодший байт результату в регістр r26
    "mov r27, r1\n"     // Переміщуємо старший байт результату в регістр r27
    "mov %A0, r26\n"    // Зберігаємо молодший байт результату в змінну %A0
    "mov %B0, R27\n"    // Зберігаємо старший байт результату в змінну %B0
    : "=&w" (result)    // Об'являємо вихідний операнд (result) як вихідний операнд зі змінною значенням
    : "r" (numb1), "r" (numb2)    // Вказуємо вхідні операнди (numb1 і numb2) як вхідні операнди регістрового типу
  );
  return result;    // Повертаємо результат множення
}

int numb1 = 0; //Змінна для зберігання множеного
int numb2 = 0; //Змінна для зберігання множника

void setup() {
  Serial.begin(9600); //Ініціалізація монітору серійного порту на частоті 9600
}

void loop() {
  Serial.print("-------------------------------\n");
  Serial.print("Enter number 1: ");
  while (!Serial.available()) {} //Перевірка вводу значення в монітор серійного порту(за відсутності у якості елементів масиву можуть записуватися нулі)
  numb1 = Serial.parseInt(); //Вводими перше число з монітору
  Serial.print(numb1);
  Serial.print("\n");
  Serial.print("Enter number 2: ");
  while (!Serial.available()) {} //Перевірка вводу значення в монітор серійного порту(за відсутності у якості елементів масиву можуть записуватися нулі)
  numb2 = Serial.parseInt(); //Вводими друге число з монітору
  Serial.print(numb2);
  Serial.print("\n");
  delay(200); //Невеличка затримка
  Serial.print("Result is: ");
  Serial.print(asm_func(numb1, numb2)); //Повертаємо результат роботи функції asm_func
  Serial.print("\n");
}
