bool x1 = 0;
bool x2 = 0;
bool x3 = 0;
bool x4 = 0;

void setup() {
  Serial.begin(9600);
  pinMode(LED_BUILTIN, OUTPUT);
  Serial.write("\n");     
  Serial.write("\n");     
  Serial.write("\n  ---------Програма роботи логічної функції---------  ");
  Serial.write("\n  --------------------------------------------------  ");
  Serial.write("\n        F = x1!x3 + !x1x3x4 + !x2!x3!x4  ");
  Serial.write("\n  --------------------------------------------------  ");
  Serial.write("\n                |        |        |                   ");
  Serial.write("\n                |        |        |                   ");
  Serial.write("\n                |        |        |                   ");
  Serial.write("\n  --------------------------------------------------  ");
  Serial.write("\n");     
}

void loop() {
  if (Serial.available() > 0) {
    Serial.print("\n  Введіть значення x1:  ");

    x1 = Serial.parseInt();
    Serial.println(x1);
    Serial.write("\n  Введіть значення x2:  ");

    x2 = Serial.parseInt();
    Serial.println(x2);
    Serial.print("\n  Введіть значення x3:  ");

    x3 = Serial.parseInt();
    Serial.println(x3);
    Serial.print("\n  Введіть значення x4:  ");

    x4 = Serial.parseInt();
    Serial.println(x4);
    logicalfunc(x1, x2, x3, x4);
    delay(5000);
  }
}

void logicalfunc(bool x1, bool x2, bool x3, bool x4) {
  if((x1 && !x3) || (!x1 && x3 && x4) || (!x2 && !x3 && !x4) == 1){
    digitalWrite(LED_BUILTIN, HIGH);
    Serial.write("\n  ------------------  ");
    Serial.write("\n F = 1");
    Serial.write("\n  ------------------  ");
    Serial.write("\n");   
  }
  else {
    digitalWrite(LED_BUILTIN, LOW);
    Serial.write("\n  ------------------  ");
    Serial.print("\n F = 0");
    Serial.write("\n  ------------------  ");
    Serial.write("\n");    
  }
}